//
//  ViewEntityBase.swift
//  AMCVVisualTemplates
//
//  Created by csharpkid on 04/01/24.
//

import Foundation

open class AMCVVTViewEntityBase {
    
    public let rawID: String?
    
    
    
    public init(rawID: String?) {
        self.rawID = rawID
    }
}
