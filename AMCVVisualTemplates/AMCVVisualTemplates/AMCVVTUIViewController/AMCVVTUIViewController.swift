//
//  AMCVVTUIViewController.swift
//  AMCVVisualTemplates
//
//  Created by csharpkid on 02/01/24.
//

import UIKit
import AMCVVisualComponents

public protocol AMCVVTUIViewControllerProtocol:UIViewController {
     var viewEntity:AMCVVTViewEntityBase? {get set}
}

open class AMCVVTUIViewController: UIViewController, AMCVVTUIViewControllerProtocol {
    public var viewEntity: AMCVVTViewEntityBase?
    

  
    
    open override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = AMCVVisualComponents.AMCVCColors.backgroundBaseColor
        self.navigationController?.navigationBar.isHidden = true
        hideKeyboardWhenTappedAround()
    }

}


fileprivate extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tapGesture.cancelsTouchesInView = false
        view.addGestureRecognizer(tapGesture)
    }

    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}
