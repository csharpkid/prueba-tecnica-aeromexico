//
//  AMCVTAnimationControllerr.swift
//  AMCVVisualTemplates
//
//  Created by csharpkid on 02/01/24.
//

import UIKit
import Lottie

public class AMCVTAnimationController: UIViewController {
    
    private var animationView: LottieAnimationView?
    private var imageView: UIImageView?
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        setupAnimation()
        setupImageView()
    }
    
    private func setupImageView() {
        // Load the image named "logo" from the same bundle
        let image = UIImage(named: "logo", in: Bundle(for: AMCVTAnimationController.self), compatibleWith: nil)
        imageView = UIImageView(image: image)
        guard let imageView = imageView else { return }
        let imageSize: CGFloat = 250
        imageView.frame = CGRect(x: 0, y: 0, width: imageSize, height: imageSize)
        imageView.center = view.center
        imageView.contentMode = .scaleAspectFit
        imageView.alpha = 0 // Start with the image view hidden
        view.addSubview(imageView)
        view.bringSubviewToFront(imageView)
    }
    
    private func setupAnimation() {
        // Assuming you have a Lottie JSON file named "launcher"
        animationView = LottieAnimationView(name: "launcher", bundle: Bundle(for: AMCVTAnimationController.self))
        guard let animationView = animationView else { return }
        let animationSize: CGFloat = 600
        animationView.frame = CGRect(x: 0, y: 0, width: animationSize, height: animationSize)
        animationView.center = view.center
        animationView.alpha = 0
        animationView.contentMode = .scaleAspectFit
        view.addSubview(animationView)
        view.bringSubviewToFront(animationView)
        
    }
    
    public class func present(from viewController: AMCVVTUIViewController,onComplete:@escaping ()->Void) {
        let modalVC = AMCVTAnimationController()
        modalVC.view.frame = viewController.view.bounds
        modalVC.modalPresentationStyle = .overCurrentContext
        viewController.present(modalVC, animated: false, completion: {
            UIView.animate(withDuration: 0.3, animations: {
                modalVC.imageView?.alpha = 1 // Fade in the image
            }, completion: { _ in
                // After the animation completes, wait for an additional 1.5 seconds, then call onComplete
                UIView.animate(withDuration: 0.5, animations: {
                    modalVC.imageView?.alpha = 0 // Fade in the image
                },completion: {_ in
                    UIView.animate(withDuration: 0.3, animations: {
                        modalVC.animationView?.alpha = 1 // Fade in the image
                    },completion: {_ in
                        modalVC.animationView?.play()
                        DispatchQueue.main.asyncAfter(wallDeadline: .now() + .milliseconds(900), execute: {
                            onComplete()
                        })
                        DispatchQueue.main.asyncAfter(wallDeadline: .now() + .milliseconds(1300), execute: {
                            modalVC.animationView?.removeFromSuperview()
                            
                            viewController.dismiss(animated: false)
                        })
                    })
                })
            })
        })
    }
}
