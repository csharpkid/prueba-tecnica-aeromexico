//
//  AMCVFavoritesManager.swift
//  AMCVFlights
//
//  Created by csharpkid on 05/01/24.
//
import Foundation
import WidgetKit

public struct FlightInfo: Codable, Equatable {
    public var flightNumber: String
    public var flightStatus:String
    public var boardingTime:String
}

open class AMCVFavoritesManager {
    public static let shared = AMCVFavoritesManager()
    private let flightInfoKey = "favoriteFlights"
    private let sharedDefaults: UserDefaults?

    private init() {
        // Initialize shared UserDefaults with your app group
        sharedDefaults = UserDefaults(suiteName: "group.cristianvillegas.com")
    }

    // Save flight info array to shared UserDefaults
    private func saveFlightInfo(_ flightInfos: [FlightInfo]) {
        let encoder = JSONEncoder()
        if let encoded = try? encoder.encode(flightInfos) {
            sharedDefaults?.set(encoded, forKey: flightInfoKey)
            WidgetCenter.shared.reloadAllTimelines()
        }
    }

    // Retrieve flight info array from shared UserDefaults
    public func getFlightInfo() -> [FlightInfo] {
        if let savedData = sharedDefaults?.object(forKey: flightInfoKey) as? Data {
            let decoder = JSONDecoder()
            if let loadedFlightInfos = try? decoder.decode([FlightInfo].self, from: savedData) {
                return loadedFlightInfos
            }
        }
        return []
    }

    // Add a new favorite flight info
    public func addFavoriteFlightInfo(_ flightInfo: FlightInfo) {
        var currentFavorites = getFlightInfo()
        if !currentFavorites.contains(flightInfo) {
            currentFavorites.append(flightInfo)
            saveFlightInfo(currentFavorites)
        }
    }

    // Remove a favorite flight info
    public func removeFavoriteFlightInfo(_ flightInfo: FlightInfo) {
        var currentFavorites = getFlightInfo()
        if let index = currentFavorites.firstIndex(of: flightInfo) {
            currentFavorites.remove(at: index)
            saveFlightInfo(currentFavorites)
        }
    }
}
