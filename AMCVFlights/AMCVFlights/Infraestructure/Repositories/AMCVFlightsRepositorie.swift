//
//  AMCVFlightsRepositorie.swift
//  AMCVFlights
//
//  Created by csharpkid on 04/01/24.
//

import Foundation
import AMCVNetwork

struct FlightStatusResponse: Codable {
    var flightStatusCollection: [FlightStatus]?
}

struct FlightStatus: Codable {
    var status: String?
    var boardingTerminal: String?
    var boardingGate: String?
    var boardingTime: String?
    var estimatedDepartureTime: String?
    var estimatedArrivalTime: String?
    var delayInMinutes: Int?
    var arrivalTerminal: String?
    var arrivalGate: String?
    var segment: Segment?
    var outGate: OutGate?
    var legType: String?
    var totalFlightTimeInMinutes: Int?
}

struct Segment: Codable {
    var segmentCode: String?
    var departureAirport: String?
    var arrivalAirport: String?
    var departureDateTime: String?
    var arrivalDateTime: String?
    var flightStatus: String?
    var operatingCarrier: String?
    var marketingCarrier: String?
    var operatingFlightCode: String?
    var marketingFlightCode: String?
    var flightDurationInMinutes: Int?
    var aircraftType: String?
    var stops: [String]? // Assuming stops is an array of strings; adjust if needed
}

struct OutGate: Codable {
    var accuracy: String?
    var dateTimeUtc: String?
    var dateTimeLocal: String?
    var sourceType: String?
}


public class AMCVFlightsRepositorie {
    
    
    static var avaibleAirPorts: [String:String] = [
        "MEX" : "Ciudad de México - AICM",
        "CUN" : "Cancún",
        "VSA" : "Villahermosa"
    ]
    
    static var avaibleCities: [String:String] = [
        "México City": "MEX" ,
        "Cancún" : "CUN"  ,
        "Villahermosa" : "VSA" 
    ]
    

    class func getByFlightNumberMock(completion: @escaping ([FlightStatus]) -> Void) {
        AMCVNetworkManager.mockData(from: "NumerodeVueloResponse", in: Bundle.init(for: AMCVFlightsRepositorie.self)) { (result: Result<FlightStatusResponse, Error>) in
            switch result {
            case .success(let response):
                // Returning the flight status collection or an empty array if nil
                completion(response.flightStatusCollection ?? [])
            case .failure:
                // Return an empty array in case of failure
                completion([])
            }
        }
    }
    
    
    class func getOriginDestinyMock(completion: @escaping ([FlightStatus]) -> Void) {
        AMCVNetworkManager.mockData(from: "OrigenDestinoResponse", in: Bundle.init(for: AMCVFlightsRepositorie.self)) { (result: Result<FlightStatusResponse, Error>) in
            switch result {
            case .success(let response):
                // Returning the flight status collection or an empty array if nil
                completion(response.flightStatusCollection ?? [])
            case .failure:
                // Return an empty array in case of failure
                completion([])
            }
        }
    }
    
}
