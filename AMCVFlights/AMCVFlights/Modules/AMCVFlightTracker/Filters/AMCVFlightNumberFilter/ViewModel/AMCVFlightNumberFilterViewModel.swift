//
//  AMCVFlightNumberFilterViewModel.swift
//  AMCVFlights
//
//  Created by csharpkid on 04/01/24.
//

import Foundation
import AMCVNavigator

class AMCVFlightNumberFilterViewModel {
    
    
    
    func formatFlightDuration(durationInMinutes: Int) -> String {
        let hours = durationInMinutes / 60
        let minutes = durationInMinutes % 60
        return "\(hours)h \(minutes)m"
    }

    func formatBoardingTime(timeString: String) -> String {
         let components = timeString.split(separator: ":")
         guard components.count >= 2 else {
             return timeString
         }
         return "\(components[0]):\(components[1])"
     }
    
    func formatTime(timeString: String) -> String {
        let isoDateFormatter = DateFormatter()
        isoDateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        isoDateFormatter.locale = Locale(identifier: "en_US_POSIX")
        isoDateFormatter.timeZone = TimeZone.current

        if let date = isoDateFormatter.date(from: timeString) {
            let timeFormatter = DateFormatter()
            timeFormatter.dateFormat = "HH:mm"
            return timeFormatter.string(from: date)
        } else {
            return timeString
        }
    }



    
    func trySearchFlightsByNumber(number:String,date:Date) {
        let favoriteFlights = AMCVFavoritesManager.shared.getFlightInfo()

        let isoDateFormatter = DateFormatter()
        isoDateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        isoDateFormatter.locale = Locale(identifier: "en_US_POSIX")
        isoDateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
        let customDateFormatter = DateFormatter()
        customDateFormatter.dateFormat = "EEEE, MMM d"
        AMCVFlightsRepositorie.getByFlightNumberMock { [weak self] flightStatuses in
                guard let self = self else { return }
                let filteredFlightStatuses = flightStatuses.filter { status in
                    guard let flightNumber = status.segment?.operatingFlightCode,
                          let departureDateString = status.segment?.departureDateTime,
                          let departureDate = isoDateFormatter.date(from: departureDateString) else {
                        return false
                    }
                    return flightNumber == number && Calendar.current.isDate(departureDate, inSameDayAs: date)
                }
            var items = filteredFlightStatuses.map { status -> AMCVFlightViewEntity in
                let isFavorite = favoriteFlights.contains(where: { $0.flightNumber == status.segment?.operatingFlightCode })
                return AMCVFlightViewEntity(
                    startTime: self.formatTime(timeString: status.estimatedDepartureTime ?? ""),
                    departCity: status.segment?.departureAirport ?? "",
                    arrivalCity: status.segment?.arrivalAirport ?? "",
                    isFavorite: isFavorite,
                    flightNumberPrefix: "AM",
                    flightNumber: status.segment?.operatingFlightCode ?? "",
                    duration: self.formatFlightDuration(durationInMinutes: status.segment?.flightDurationInMinutes ?? 0),
                    flightStatus: .init(rawValue: status.segment?.flightStatus ?? "") ?? .arrived,
                    arrivalTime: self.formatTime(timeString: status.estimatedArrivalTime ?? ""), date: customDateFormatter.string(from: date),
                    boardingTime: self.formatBoardingTime(timeString: status.boardingTime ?? ""), departureTerminal: status.boardingTerminal ?? "", arrivalTerminal: status.arrivalTerminal ?? "", boardingGate: status.boardingGate ?? "",
                    departAirport: AMCVFlightsRepositorie.avaibleAirPorts[status.segment?.departureAirport ?? ""] ?? "", arrivalAirport: AMCVFlightsRepositorie.avaibleAirPorts[status.segment?.arrivalAirport ?? ""] ?? ""
                )
            }
            items.sort(by: { $0.isFavorite && !$1.isFavorite })
                let viewEntity = AMCVFlightsViewEntity(
                    rawID: nil,
                    items: items,
                    title: "AM \(number)",
                    dateSubtitle: customDateFormatter.string(from: date),
                    directionSubtitle: items.count == 0 ?  "" : "\(items[0].departAirport) to \(items[0].arrivalAirport)"
                )

                AMCVNavigatorCore.startFlow(with: "AMCVFlights", mode: .push, viewEntity: viewEntity)
            }

        
    }
    
    
}
