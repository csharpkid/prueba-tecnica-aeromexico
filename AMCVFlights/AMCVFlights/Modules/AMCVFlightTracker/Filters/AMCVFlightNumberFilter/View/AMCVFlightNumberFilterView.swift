//
//  AMCVDestinationFilterView.swift
//  AMCVFlights
//
//  Created by csharpkid on 03/01/24.
//

import Foundation
import UIKit
import AMCVVisualComponents



class AMCVFlightNumberFilterView: UIView, AMCVCVTextfieldDelegate {
    
    var numberSelected:String? = nil
    var dateSelected:Date? = nil
    
    func numberDidChange(_ number: String) {
        numberSelected = number
    }
    
    func dateDidSelect(_ date: Date) {
        dateSelected = date

    }
    

    var viewModel: AMCVFlightNumberFilterViewModel? =  AMCVFlightNumberFilterViewModel()  {
         didSet {
             updateViewFromViewModel()
         }
     }
     
    private func updateViewFromViewModel() {
    }
    
    
    var contentView: UIView!
    

    @IBOutlet weak var descriptionLabel:AMCVVCLabel!
    
    @IBOutlet weak var flightNumberTextfield:AMCVCVTextfield!
    @IBOutlet weak var flightDateTextfield:AMCVCVTextfield!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    @objc func handleTapOnLabel(_ gesture: UITapGestureRecognizer) {
        let location = gesture.location(in: descriptionLabel)
        guard let text = descriptionLabel.attributedText?.string else { return }
        let layoutManager = NSLayoutManager()
        let textContainer = NSTextContainer(size: CGSize.zero)
        let textStorage = NSTextStorage(attributedString: descriptionLabel.attributedText!)
        layoutManager.addTextContainer(textContainer)
        textStorage.addLayoutManager(layoutManager)
        textContainer.lineFragmentPadding = 0.0
        textContainer.lineBreakMode = descriptionLabel.lineBreakMode
        textContainer.maximumNumberOfLines = descriptionLabel.numberOfLines
        let textBoundingBox = layoutManager.usedRect(for: textContainer)
        textContainer.size = textBoundingBox.size
        let characterIndex = layoutManager.characterIndex(for: location, in: textContainer, fractionOfDistanceBetweenInsertionPoints: nil)
        if let range = text.range(of: "destination"),
           NSLocationInRange(characterIndex, NSRange(range, in: text)) {
            print("Tapped on 'destination'")
        }
    }

    func configureDescriptionLabel() {
        let fullText = "Can’t find your flight number?\nTry searching by destination"
        let boldText = "destination"
        let attributedString = NSMutableAttributedString(string: fullText)
        if let boldTextRange = fullText.range(of: boldText) {
            let nsRange = NSRange(boldTextRange, in: fullText)
            attributedString.addAttribute(.font, value: UIFont.boldSystemFont(ofSize: descriptionLabel.font.pointSize), range: nsRange)
            attributedString.addAttribute(.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: nsRange)
        }
        descriptionLabel.attributedText = attributedString
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleTapOnLabel(_:)))
          descriptionLabel.isUserInteractionEnabled = true
          descriptionLabel.addGestureRecognizer(tapGesture)
    }

    
    func configureFonts() {
        descriptionLabel.customFontSize = 12
        descriptionLabel.fontStyle = 0
    }
    
    private func commonInit() {
        let bundle = Bundle(for: AMCVFlightNumberFilterView.self)
        let nib = UINib(nibName: "AMCVFlightNumberFilterView", bundle: bundle)
        contentView = nib.instantiate(withOwner: self, options: nil).first as? UIView
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        self.configureFonts()
        self.configureDescriptionLabel()
        self.contentView.backgroundColor = AMCVCColors.backgroundBaseColor
        flightNumberTextfield.delegate = self
        flightDateTextfield.delegate = self
    }
    @IBAction func trySearch() {
        guard let viewModel = viewModel else { return }
        viewModel.trySearchFlightsByNumber(number: self.numberSelected ?? "", date: self.dateSelected ?? Date())
        }
        
    
    
}
