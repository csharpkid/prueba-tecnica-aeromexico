//
//  AMCVDestinationFilterView.swift
//  AMCVFlights
//
//  Created by csharpkid on 03/01/24.
//

import Foundation
import UIKit
import AMCVVisualComponents

class AMCVDestinationFilterView: UIView ,AMCVCVTextfieldDelegate {
    
    var numberSelected:String? = nil
    var dateSelected:Date? = nil
    
    func numberDidChange(_ number: String) {
        numberSelected = number
    }
    
    func dateDidSelect(_ date: Date) {
        dateSelected = date
    }
    
    var contentView: UIView!
    
    
    @IBOutlet weak var descriptionLabel:AMCVVCLabel!
    @IBOutlet weak var flightDateTextfield:AMCVCVTextfield!
    @IBOutlet weak var originPickerTextfield:AMCVCVTextfield!
    @IBOutlet weak var destinyPickerTextfield:AMCVCVTextfield!
    
    var viewModel: AMCVDestinationFilterViewModel? = nil {
        didSet {
            updateViewFromViewModel()
        }
    }
    
    
    
    private func updateViewFromViewModel(firstConfiguration:Bool? = true) {
        guard let viewModel = viewModel else { return }
        configurePickerTextField(originPickerTextfield, withItems: viewModel.filteredOriginData, prefixeds: viewModel.prefixies,firstConfiguration: firstConfiguration)
        configurePickerTextField(destinyPickerTextfield, withItems: viewModel.filteredDestinationData, prefixeds: viewModel.prefixies,firstConfiguration:firstConfiguration)
    }

   
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    @objc func handleTapOnLabel(_ gesture: UITapGestureRecognizer) {
        let location = gesture.location(in: descriptionLabel)
        guard let text = descriptionLabel.attributedText?.string else { return }
        let layoutManager = NSLayoutManager()
        let textContainer = NSTextContainer(size: CGSize.zero)
        let textStorage = NSTextStorage(attributedString: descriptionLabel.attributedText!)
        layoutManager.addTextContainer(textContainer)
        textStorage.addLayoutManager(layoutManager)
        textContainer.lineFragmentPadding = 0.0
        textContainer.lineBreakMode = descriptionLabel.lineBreakMode
        textContainer.maximumNumberOfLines = descriptionLabel.numberOfLines
        let textBoundingBox = layoutManager.usedRect(for: textContainer)
        textContainer.size = textBoundingBox.size
        let characterIndex = layoutManager.characterIndex(for: location, in: textContainer, fractionOfDistanceBetweenInsertionPoints: nil)
        if let range = text.range(of: "flight number"),
           NSLocationInRange(characterIndex, NSRange(range, in: text)) {
            print("Tapped on 'number'")
        }
    }

    func configureDescriptionLabel() {
        let fullText = "Looking for a specific flight?\nTry searching by flight number"
        let boldText = "flight number"
        let attributedString = NSMutableAttributedString(string: fullText)
        if let boldTextRange = fullText.range(of: boldText) {
            let nsRange = NSRange(boldTextRange, in: fullText)
            attributedString.addAttribute(.font, value: UIFont.boldSystemFont(ofSize: descriptionLabel.font.pointSize), range: nsRange)
            attributedString.addAttribute(.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: nsRange)
        }
        descriptionLabel.attributedText = attributedString
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleTapOnLabel(_:)))
          descriptionLabel.isUserInteractionEnabled = true
          descriptionLabel.addGestureRecognizer(tapGesture)
    }
    
    func configureFonts() {
        descriptionLabel.customFontSize = 12
        descriptionLabel.fontStyle = 0
    }
    
    
    @IBAction func trySearch() {
        guard let viewModel = viewModel else { return }
        viewModel.trySearchFlightsByOriginAnDestiny(date: self.dateSelected ?? Date())
    }
    
    
    private func configurePickerTextField(_ textField: AMCVCVTextfield, withItems items: [String],prefixeds:[String],firstConfiguration:Bool? = true) {
        textField.configurePickerViewDataSource(with: items,firstConfiguration: firstConfiguration) { selectedIndex in
            textField.changePrefix(to: self.viewModel?.findPrefix(city: items[selectedIndex]) ?? "")
      
            switch textField {
            case self.originPickerTextfield:
            self.viewModel?.selectedOrigin =  self.viewModel?.findPrefix(city: items[selectedIndex]) ?? ""
            self.viewModel?.selectedCityOrigin = items[selectedIndex]
                self.viewModel?.onSelectedField(completion: {
                    self.updateViewFromViewModel(firstConfiguration: false)
                })
            break
            case self.destinyPickerTextfield:
            self.viewModel?.selectedDestiny =  self.viewModel?.findPrefix(city: items[selectedIndex]) ?? ""
            self.viewModel?.selectedCityDestiny = items[selectedIndex]
            self.viewModel?.onSelectedField(completion: {
                self.updateViewFromViewModel(firstConfiguration: false)
            })
            break
            default:
            break
            }
   
        
        }
    }
    
    private func commonInit() {
        let bundle = Bundle(for: AMCVDestinationFilterView.self)
        let nib = UINib(nibName: "AMCVDestinationFilterView", bundle: bundle)
        contentView = nib.instantiate(withOwner: self, options: nil).first as? UIView
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        self.configureFonts()
        self.configureDescriptionLabel()
        self.contentView.backgroundColor = AMCVCColors.backgroundBaseColor
        self.viewModel = AMCVDestinationFilterViewModel()
        flightDateTextfield.delegate = self
    }
    
}
