//
//  AMCVDestinationFilter.swift
//  AMCVFlights
//
//  Created by csharpkid on 04/01/24.
//

import Foundation
import AMCVNavigator

public class AMCVDestinationFilterViewModel {
    
    
    var filteredOriginData: [String] = [] // Populate with actual data
    var filteredDestinationData: [String] = [] // Populate with actual data
    
    var originData: [String] = ["México City","Cancún","Villahermosa"] // Populate with actual data
    var destinationData: [String] = ["México City","Cancún","Villahermosa"] // Populate with actual data
    var prefixies: [String] = ["MEX","CUN","VSA"] // Populate with actual data
    
    var selectedOrigin: String = ""
    var selectedCityOrigin: String = ""
    
    var selectedCityDestiny: String = ""
    var selectedDestiny: String = ""

    public init() {
        filteredOriginData = self.originData
        filteredDestinationData = self.destinationData
    }
    
    func formatBoardingTime(timeString: String) -> String {
         let components = timeString.split(separator: ":")
         guard components.count >= 2 else {
             return timeString // Return original if format is not as expected
         }
         return "\(components[0]):\(components[1])"
     }
        
        
        func formatFlightDuration(durationInMinutes: Int) -> String {
            let hours = durationInMinutes / 60
            let minutes = durationInMinutes % 60
            return "\(hours)h \(minutes)m"
        }

        
    func formatTime(timeString: String) -> String {
        let isoDateFormatter = DateFormatter()
        isoDateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        isoDateFormatter.locale = Locale(identifier: "en_US_POSIX")
        isoDateFormatter.timeZone = TimeZone.current // Use the current time zone

        if let date = isoDateFormatter.date(from: timeString) {
            let timeFormatter = DateFormatter()
            timeFormatter.dateFormat = "HH:mm"
            return timeFormatter.string(from: date)
        } else {
            return timeString // Return original if format is not as expected
        }
    }
    func findPrefix(city:String)->String? {
        let avaibleCity = AMCVFlightsRepositorie.avaibleCities[city]
        return avaibleCity
    }

    func onSelectedField(completion: @escaping ()->Void) {
        filteredOriginData = self.originData.filter({ i in
            return self.selectedCityDestiny != i
        })
        
        filteredDestinationData = self.destinationData.filter({ i in
            return selectedCityOrigin != i
        })
        completion()
    }
        
        func trySearchFlightsByOriginAnDestiny(date:Date) {
            // Fetch the list of favorite flights
            let favoriteFlights = AMCVFavoritesManager.shared.getFlightInfo()
            let isoDateFormatter = DateFormatter()
            isoDateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss" // Adjust if your format differs
            isoDateFormatter.locale = Locale(identifier: "en_US_POSIX") // Use the appropriate locale
            isoDateFormatter.timeZone = TimeZone(secondsFromGMT: 0) // Adjust if the timezone is included in your JSON dates
            let customDateFormatter = DateFormatter()
            customDateFormatter.dateFormat = "EEEE, MMM d" // Example format for dateSubtitle
            // other customDateFormatter configurations...
            AMCVFlightsRepositorie.getOriginDestinyMock { [weak self] flightStatuses in
                    guard let self = self else { return }

                    let filteredFlightStatuses = flightStatuses.filter { status in
                        guard let departureAirport = status.segment?.departureAirport,
                              let arrivalAirport = status.segment?.arrivalAirport,
                              let departureDateString = status.segment?.departureDateTime,
                              let departureDate = isoDateFormatter.date(from: departureDateString) else {
                            return false
                        }
                        return (departureAirport == self.selectedOrigin && arrivalAirport == self.selectedDestiny) && Calendar.current.isDate(departureDate, inSameDayAs: date)
                    }

                var items = filteredFlightStatuses.map { status -> AMCVFlightViewEntity in
                    // Create and return AMCVFlightViewEntity based on the status
                    // Replace the following with actual data mapping from status
                    let isFavorite = favoriteFlights.contains(where: { $0.flightNumber == status.segment?.operatingFlightCode })
                    return AMCVFlightViewEntity(
                        startTime: self.formatTime(timeString: status.estimatedDepartureTime ?? ""),
                        departCity: status.segment?.departureAirport ?? "",
                        arrivalCity: status.segment?.arrivalAirport ?? "",
                        isFavorite: isFavorite, // Set as needed
                        flightNumberPrefix: "AM", // Replace with actual prefix
                        flightNumber: status.segment?.operatingFlightCode ?? "",
                        duration: self.formatFlightDuration(durationInMinutes: status.segment?.flightDurationInMinutes ?? 0), // Calculate or extract duration
                        flightStatus: .init(rawValue: status.segment?.flightStatus ?? "") ?? .arrived,
                        arrivalTime: self.formatTime(timeString: status.estimatedArrivalTime ?? ""), date: customDateFormatter.string(from: date), boardingTime: self.formatBoardingTime(timeString: status.boardingTime ?? ""), departureTerminal: status.boardingTerminal ?? "", arrivalTerminal: status.boardingTerminal ?? "", boardingGate: status.boardingGate ?? "",
                        departAirport: AMCVFlightsRepositorie.avaibleAirPorts[status.segment?.departureAirport ?? ""] ?? "", arrivalAirport: AMCVFlightsRepositorie.avaibleAirPorts[status.segment?.arrivalAirport ?? ""] ?? ""
                    )
                }
                items.sort(by: { $0.isFavorite && !$1.isFavorite })

                    let viewEntity = AMCVFlightsViewEntity(
                        rawID: nil,
                        items: items,
                        title: "\(self.selectedOrigin) → \(self.selectedDestiny)",
                        dateSubtitle: customDateFormatter.string(from: date),
                        directionSubtitle: "\(self.selectedCityOrigin) to \(self.selectedCityDestiny)"
                    )

                    AMCVNavigatorCore.startFlow(with: "AMCVFlights", mode: .push, viewEntity: viewEntity)
                }

            
        }
        
        
    
    
    
}
