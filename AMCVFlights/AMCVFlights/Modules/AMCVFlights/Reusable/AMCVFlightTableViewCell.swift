//
//  AMCVFlightTableViewCell.swift
//  AMCVFlights
//
//  Created by csharpkid on 04/01/24.
//

import UIKit
import AMCVVisualComponents
import AMCVNavigator

class AMCVFlightTableViewCell: UITableViewCell {

    
    var item:AMCVFlightViewEntity? = nil {
        didSet {
            self.flightPrefixNumberLabel.text = "\(item?.flightNumberPrefix ?? "")\(item?.flightNumber ?? "")"
            self.arrivalCityLabel.text = item?.arrivalCity
            self.arrivalTimeLabel.text = item?.arrivalTime
            self.departTimeLabel.text = item?.startTime
            self.hoursUILabel.text = item?.duration
            self.departCityLabel.text = item?.departCity
            self.favoriteUISwitch.isOn = item?.isFavorite ?? false
            switch item?.flightStatus {
            case .arrived:
                item?.flightProgress = 0.91
                self.progressBar.progress = item?.flightProgress ?? 0.08
            self.statusUIImageView.image = UIImage.init(vcName: "AMCVVCArrivedPill")
            break
            case .onTime:
                item?.flightProgress = 0.08
                self.progressBar.progress = item?.flightProgress ?? 0.08
            self.statusUIImageView.image = UIImage.init(vcName: "AMCVVCOnTimePill")
            break
            case .delayed:
                self.progressBar.progress = 0.08
                self.progressBar.progress = item?.flightProgress ?? 0.08
            self.statusUIImageView.image = UIImage.init(vcName: "AMCVVCDelayedPill")
            break
            case nil:
            break
            }
            
        }
    }
    
    @IBOutlet weak var favoriteUISwitch:UISwitch!
    @IBOutlet weak var cellView:UIView!
    @IBOutlet weak var statusUIImageView:UIImageView!
    @IBOutlet weak var rightChevronUIImageView:UIImageView!
    @IBOutlet weak var progressBar:AMCVVCProgressBar!
    
    @IBOutlet weak var hoursUILabel:UILabel!
    
    @IBOutlet weak var arrivalTimeLabel:AMCVVCLabel!
    @IBOutlet weak var departTimeLabel:AMCVVCLabel!
    
    @IBOutlet weak var departCityLabel:AMCVVCLabel!
    
    @IBOutlet weak var arrivalCityLabel:AMCVVCLabel!
    

    @IBOutlet weak var flightPrefixNumberLabel:AMCVVCLabel!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        cellView.layer.borderColor = AMCVCColors.tintColor.cgColor
        cellView.layer.borderWidth = 2
        cellView.layer.masksToBounds = true
        cellView.layer.cornerRadius = 14
        favoriteUISwitch.transform = CGAffineTransform(scaleX: 0.6, y: 0.6)
        self.statusUIImageView.image = UIImage.init(vcName: "AMCVVCDelayedPill")
        self.rightChevronUIImageView.image = UIImage.init(vcName: "AMCVVCRightChevron")
        self.hoursUILabel.textColor = AMCVCColors.semiDisabledTextColor
        setupTapGesture()

    }

    @IBAction func onSwitched(_ sender: UISwitch) {
        guard let flightNumber = item?.flightNumber else {
            return
        }
        item?.isFavorite = sender.isOn
        if sender.isOn {
            AMCVFavoritesManager.shared.addFavoriteFlightInfo(.init(flightNumber: flightNumber, flightStatus: self.item?.flightStatus.rawValue ?? "", boardingTime: self.item?.boardingTime ?? ""))
            postCustomNotification(withTitle: "Flight Added to Favorites", body: "Your flight has been successfully added to your favorites.")
        } else {
            AMCVFavoritesManager.shared.removeFavoriteFlightInfo(.init(flightNumber: flightNumber, flightStatus: self.item?.flightStatus.rawValue  ?? "", boardingTime: self.item?.boardingTime ?? ""))
            postCustomNotification(withTitle: "Flight Removed from Favorites", body: "Your flight has been removed from your favorites.")
        }
    }

    
    func postCustomNotification(withTitle title: String, body: String) {
        let userInfo = [
            "title": title,
            "body": body
        ]
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "customNotification"), object: nil, userInfo: userInfo)
    }
    
    
    private func setupTapGesture() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(onTapped))
        cellView.addGestureRecognizer(tapGesture)
    }
    
    @objc func onTapped() {
        AMCVNavigatorCore.startFlow(with: "AMCVFlightDetail", mode: .push,viewEntity: item)
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
