//
//  AMCVFlightTableViewEntity.swift
//  AMCVFlights
//
//  Created by csharpkid on 04/01/24.
//

import Foundation
import AMCVVisualTemplates

public enum AMCVFlightStatusType:String {
    case arrived = "ARRIVED"
    case onTime = "ON_TIME"
    case delayed = "DELAYED"
    
}

class AMCVFlightViewEntity: AMCVVTViewEntityBase {
    let startTime: String
    let arrivalTime: String
    let departCity: String
    let arrivalCity: String
    var isFavorite: Bool
    let flightNumberPrefix: String
    let flightNumber: String
    let duration: String
    let date: String
    let boardingTime: String
    let departureTerminal: String
    let arrivalTerminal: String
    let boardingGate: String
    let departAirport: String
    let arrivalAirport: String
    let flightStatus: AMCVFlightStatusType
    var flightProgress: Float? = nil

    init(startTime: String,
         departCity: String,
         arrivalCity: String,
         isFavorite: Bool,
         flightNumberPrefix: String,
         flightNumber: String,
         duration: String,
         flightStatus: AMCVFlightStatusType,
         arrivalTime: String,
         date: String,
         boardingTime: String,
         departureTerminal: String,
         arrivalTerminal: String,
         boardingGate: String,
         departAirport: String,
         arrivalAirport: String) {
        self.startTime = startTime
        self.departCity = departCity
        self.arrivalCity = arrivalCity
        self.isFavorite = isFavorite
        self.flightNumberPrefix = flightNumberPrefix
        self.flightNumber = flightNumber
        self.duration = duration
        self.flightStatus = flightStatus
        self.arrivalTime = arrivalTime
        self.date = date
        self.boardingTime = boardingTime
        self.departureTerminal = departureTerminal
        self.arrivalTerminal = arrivalTerminal
        self.boardingGate = boardingGate
        self.departAirport = departAirport
        self.arrivalAirport = arrivalAirport
        super.init(rawID: nil)
    }
}
