//
//  AMCVFlightsDefinition.swift
//  AMCVFlights
//
//  Created by csharpkid on 02/01/24.
//

import Foundation
import AMCVNavigator

open class AMCVFlightsDefinition:AMCVNavigableFlowsProtocol {
    
    public static func registerFlows() {
        AMCVNavigatorCore.registerFlow(flowName: "AMCVFlightTracker", for: AMCVFlightTrackerRouter.createModule())
        AMCVNavigatorCore.registerFlow(flowName: "AMCVFlights", for: AMCVFlightsRouter.createModule())     
        AMCVNavigatorCore.registerFlow(flowName: "AMCVFlightDetail", for: AMCVFlightDetailRouter.createModule())
    }
    
}



