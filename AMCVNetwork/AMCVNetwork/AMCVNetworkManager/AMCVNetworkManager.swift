//
//  AMCVNetworkManager.swift
//  AMCVNetwork
//
//  Created by csharpkid on 04/01/24.
//

import Foundation



open class AMCVNetworkManager {

    class func performRequest<T: Codable, U: Codable>(with url: URL, requestBody: U?, completion: @escaping (Result<T, Error>) -> Void) {
        let task = URLSession.shared.dataTask(with: url) { data, response, error in
            guard let data = data, error == nil else {
                completion(.failure(error ?? URLError(.badServerResponse)))
                return
            }
            do {
                let decodedResponse = try JSONDecoder().decode(T.self, from: data)
                completion(.success(decodedResponse))
            } catch {
                completion(.failure(error))
            }
        }
        task.resume()
    }

    public class func mockData<T: Codable>(from jsonFileName: String, in bundle: Bundle, completion: @escaping (Result<T, Error>) -> Void) {
        guard let url = bundle.url(forResource: jsonFileName, withExtension: "json") else {
            completion(.failure(URLError(.fileDoesNotExist)))
            return
        }
        do {
            let data = try Data(contentsOf: url)
            let decodedData = try JSONDecoder().decode(T.self, from: data)
            completion(.success(decodedData))
        } catch {
            completion(.failure(error))
        }
    }
}
