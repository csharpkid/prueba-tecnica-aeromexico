//
//  AMCVNavigableFlowProtocol.swift
//  AMCVNavigator
//
//  Created by csharpkid Mac on 02/01/24.
//

import Foundation


public protocol AMCVNavigableFlowsProtocol {
    static func registerFlows()
}

