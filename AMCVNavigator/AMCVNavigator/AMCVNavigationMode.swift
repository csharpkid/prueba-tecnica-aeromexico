//
//  Utils.swift
//  AMCVNavigator
//
//  Created by csharpkid Mac on 02/01/24.
//

import Foundation

public enum AMCVNavigationMode {
    case modal
    case initialContext
    case push
}
