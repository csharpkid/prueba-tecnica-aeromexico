//
//  AMCVNavigatorCore.swift
//  AMCVNavigator
//
//  Created by csharpkid on 02/01/24.
import Foundation
import UIKit
#if canImport(AMCVVisualTemplates)
import AMCVVisualTemplates
#endif

public class AMCVNavigatorCore {

    // A static dictionary to map flow identifiers to specific UIViewController instances
    private static var flowRegistry: [String: UIViewController] = [:]

    // Prevents instantiation
    private init() {}

    /// Registers a navigation flow with a specific identifier.
    /// - Parameters:
    ///   - flowName: A string identifier for the navigation flow.
    ///   - viewController: The UIViewController associated with the flow.
    open class func registerFlow(flowName: String, for viewController: UIViewController) {
        flowRegistry[flowName] = viewController
    }

    /// Retrieves a registered UIViewController for a specific flow identifier.
      /// - Parameter flowIdentifier: A string identifier for the navigation flow.
      /// - Returns: An optional UIViewController associated with the flow.
      open class func getRegisteredFlow(for flowIdentifier: String) -> UIViewController? {
          return flowRegistry[flowIdentifier]
      }

    
    /// Starts a navigation flow identified by a string in the specified mode.
    /// - Parameters:
    ///   - flowIdentifier: A string identifier for the navigation flow.
    ///   - mode: The mode in which the flow should start. This can be modal, initialContext, or push.
    open class func startFlow(with flowIdentifier: String, mode: AMCVNavigationMode) {
        guard let viewController = flowRegistry[flowIdentifier] else {
            print("No view controller registered for \(flowIdentifier)")
            return
        }


        switch mode {
        case .modal:
            startAsModal(viewController)
        case .initialContext:
            startWithInitialContext(viewController)
        case .push:
            startAsPush(viewController)
        }
    }

#if canImport(AMCVVisualTemplates)
    open class func startFlow(with flowIdentifier: String, mode: AMCVNavigationMode,viewEntity:  AMCVVTViewEntityBase? = nil) {
        guard let viewController = flowRegistry[flowIdentifier] else {
            print("No view controller registered for \(flowIdentifier)")
            return
        }
        if let viewControllerBase = viewController as? AMCVVTUIViewController {
            viewControllerBase.viewEntity = viewEntity
        }

        switch mode {
        case .modal:
            startAsModal(viewController)
        case .initialContext:
            startWithInitialContext(viewController)
        case .push:
            startAsPush(viewController)
        }
    }
    #endif
    
    // MARK: - Private Helper Methods

    private class func startAsModal(_ viewController: UIViewController) {
        DispatchQueue.main.async {
            viewController.modalPresentationStyle = .overCurrentContext
            viewController.modalTransitionStyle = .crossDissolve
            if let rootVC = UIApplication.shared.windows.first(where: { $0.isKeyWindow })?.rootViewController {
                rootVC.present(viewController, animated: true, completion: nil)
            }
        }
    }

    private class func startWithInitialContext(_ viewController: UIViewController) {
        DispatchQueue.main.async {
            if let window = UIApplication.shared.windows.first(where: { $0.isKeyWindow }) {
                window.rootViewController = viewController
            }
        }
    }
    private class func startAsPush(_ viewController: UIViewController) {
        DispatchQueue.main.async {
            if let navigationController = UIApplication.shared.windows.first(where: { $0.isKeyWindow })?.rootViewController as? UINavigationController {
                navigationController.delegate = navigationController // Set the delegate
                navigationController.pushViewController(viewController, animated: true)
            }
        }
    }

}

class SlideRightAnimator: NSObject, UIViewControllerAnimatedTransitioning {
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.2 // Duration of the animation
    }

    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        guard let toVC = transitionContext.viewController(forKey: .to),
              let fromVC = transitionContext.viewController(forKey: .from) else {
            return
        }

        let containerView = transitionContext.containerView
        containerView.insertSubview(toVC.view, aboveSubview: fromVC.view)
        toVC.view.frame = fromVC.view.frame

        // Start off-screen (right side)
        toVC.view.transform = CGAffineTransform(translationX: toVC.view.frame.width, y: 0)

        UIView.animate(withDuration: transitionDuration(using: transitionContext), animations: {
            // Move into view
            toVC.view.transform = CGAffineTransform.identity
        }, completion: { _ in
            transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
        })
    }
}

class SlideLeftAnimator: NSObject, UIViewControllerAnimatedTransitioning {
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.2 // Duration of the animation
    }

    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        guard let toVC = transitionContext.viewController(forKey: .to),
              let fromVC = transitionContext.viewController(forKey: .from) else {
            return
        }

        let containerView = transitionContext.containerView
        containerView.insertSubview(toVC.view, aboveSubview: fromVC.view)
        toVC.view.frame = fromVC.view.frame

        // Start off-screen (left side)
        toVC.view.transform = CGAffineTransform(translationX: -toVC.view.frame.width, y: 0)

        UIView.animate(withDuration: transitionDuration(using: transitionContext), animations: {
            // Move into view
            toVC.view.transform = CGAffineTransform.identity
        }, completion: { _ in
            transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
        })
    }
}


extension UINavigationController: UINavigationControllerDelegate {
    public func navigationController(_ navigationController: UINavigationController, animationControllerFor operation: UINavigationController.Operation, from fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        if operation == .push {
            return SlideLeftAnimator()
        }
        return nil // Use default animation for other operations
    }
}
