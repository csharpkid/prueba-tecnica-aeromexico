//
//  AMCVCVLabel.swift
//  AMCVVisualComponents
//
//  Created by csharpkid on 03/01/24.
//
import UIKit

@IBDesignable
open class AMCVVCLabel: UILabel {

    enum FontStyle: Int {
        case regular = 0
        case semiBold = 1
        case bold = 2

        var fontName: String {
            switch self {
            case .regular:
                return "GarnettRegular-Regular"
            case .semiBold:
                return "GarnettSemibold-Regular"
            case .bold:
                return "Garnett-Bold"
            }
        }
    }

    @IBInspectable public var fontStyle: Int = 0 {
        didSet {
            updateFont()
        }
    }

    @IBInspectable public var customFontSize: CGFloat = 14 {
        didSet {
            updateFont()
        }
    }

    private func updateFont() {
        guard let selectedFontStyle = FontStyle(rawValue: fontStyle) else {
            print("Invalid font style index")
            return
        }

        registerFont(fontName: selectedFontStyle.fontName)
        self.font = UIFont(name: selectedFontStyle.fontName, size: customFontSize)

        if self.font == nil {
            print("Failed to load font: \(selectedFontStyle.fontName). Make sure the font file is included and the name is correct.")
        }
    }

    private func registerFont(fontName: String) {
        guard let fontURL = Bundle(for: AMCVVCLabel.self).url(forResource: fontName, withExtension: "ttf"),
              let fontDataProvider = CGDataProvider(url: fontURL as CFURL),
              let font = CGFont(fontDataProvider) else {
            print("Could not load font URL for: \(fontName)")
            return
        }

        var error: Unmanaged<CFError>?
        if !CTFontManagerRegisterGraphicsFont(font, &error), let error = error?.takeRetainedValue() {
            if CFErrorGetCode(error) != CTFontManagerError.alreadyRegistered.rawValue {
            }
        }
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        updateFont()
    }

    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        updateFont()
    }

    open override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        updateFont()
    }
}
