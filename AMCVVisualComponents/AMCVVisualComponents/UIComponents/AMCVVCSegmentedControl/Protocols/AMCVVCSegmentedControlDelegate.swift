//
//  AMCVVCSegmentedControlDelegate.swift
//  AMCVVisualComponents
//
//  Created by csharpkid on 02/01/24.
//

import Foundation

public protocol AMCVVCSegmentedControlDelegate: AnyObject {
    func segmentedControlDidChange(_ sender: AMCVVCSegmentedControl, selectedSegmentIndex: Int)
}
