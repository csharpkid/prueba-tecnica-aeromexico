//
//  AMCVVCSegmentedControl.swift
//  AMCVVisualComponents
//
//  Created by csharpkid on 02/01/24.
//

import Foundation
import UIKit
public class AMCVVCSegmentedControl: UIView {

    public weak var delegate: AMCVVCSegmentedControlDelegate?
    private let segmentedControl = UISegmentedControl()
    public var padding: UIEdgeInsets
    public var selectedIndex = 0

    init(frame: CGRect, padding: UIEdgeInsets = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)) {
        self.padding = padding
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder: NSCoder) {
        self.padding = UIEdgeInsets(top: 4, left: 4, bottom: 4, right: 4)
        super.init(coder: coder)
        commonInit()
    }

    private func commonInit() {
        addSubview(segmentedControl)
        setupSegmentedControlConstraints()
        configureSegmentedControl()
    }

    private func setupSegmentedControlConstraints() {
        segmentedControl.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            segmentedControl.topAnchor.constraint(equalTo: topAnchor, constant: padding.top),
            segmentedControl.leadingAnchor.constraint(equalTo: leadingAnchor, constant: padding.left),
            segmentedControl.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -padding.right),
            segmentedControl.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -padding.bottom)
        ])
    }

    private func configureSegmentedControl() {
        segmentedControl.addTarget(self, action: #selector(segmentChanged(_:)), for: .valueChanged)
        
        self.layer.cornerRadius = 4
        self.layer.masksToBounds = true
        self.layer.borderWidth = 1
        self.layer.borderColor = AMCVCColors.borderColor.cgColor
        
        configureTextColors()
        configureFonts()
        configureColors()
    }
    private func configureFonts() {
        let attributes: [NSAttributedString.Key: Any] = [
            .font: UIFont.amcvvcFont(style: .semiBold, size: 12) ?? UIFont.systemFont(ofSize: 12),
                // You can also set other attributes like .foregroundColor here if needed
            ]
        self.segmentedControl.setTitleTextAttributes(attributes, for: .normal)
    }
    private func configureTextColors() {
        let normalTextAttributes: [NSAttributedString.Key: Any] = [.foregroundColor: UIColor.black]
        let selectedTextAttributes: [NSAttributedString.Key: Any] = [.foregroundColor: UIColor.white]
        segmentedControl.setTitleTextAttributes(normalTextAttributes, for: .normal)
        segmentedControl.setTitleTextAttributes(selectedTextAttributes, for: .selected)
    }

    private func configureColors() {
        backgroundColor = AMCVCColors.backgroundBaseColor
        segmentedControl.selectedSegmentTintColor = AMCVCColors.tintColor
        segmentedControl.backgroundColor = AMCVCColors.backgroundBaseColor
        fixBackgroundSegmentControl(segmentedControl)
    }

    public func configure(with options: [String]) {
        segmentedControl.removeAllSegments()
        for (index, option) in options.enumerated() {
            segmentedControl.insertSegment(withTitle: option, at: index, animated: false)
        }
        segmentedControl.selectedSegmentIndex = 0
    }

    @objc private func segmentChanged(_ sender: UISegmentedControl) {
        self.selectedIndex = sender.selectedSegmentIndex
        delegate?.segmentedControlDidChange(self, selectedSegmentIndex: sender.selectedSegmentIndex)
    }

    func fixBackgroundSegmentControl(_ segmentControl: UISegmentedControl) {
        if #available(iOS 13.0, *) {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                for i in 0...(segmentControl.numberOfSegments - 1) {
                    let backgroundSegmentView = segmentControl.subviews[i]
                    backgroundSegmentView.isHidden = true
                }
            }
        }
    }

    public func setPadding(_ newPadding: UIEdgeInsets) {
        padding = newPadding
        setupSegmentedControlConstraints()
    }
    public func setSelectedIndex(_ index: Int) {
           guard index >= 0 && index < segmentedControl.numberOfSegments else {
               print("Index out of range")
               return
           }

           // Update the selectedIndex property
           selectedIndex = index
           // Update the UISegmentedControl's selected index
           segmentedControl.selectedSegmentIndex = index
           // Notify the delegate about the change
           delegate?.segmentedControlDidChange(self, selectedSegmentIndex: index)
       }
    
}
