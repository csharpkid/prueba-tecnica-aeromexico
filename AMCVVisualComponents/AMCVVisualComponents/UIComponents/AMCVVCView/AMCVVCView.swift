//
//  AMCVVCView.swift
//  AMCVVisualComponents
//
//  Created by csharpkid on 05/01/24.
//

import Foundation
import UIKit

@IBDesignable
class AMCVVCView: UIView {

    // Inspectable property for corner radius
    @IBInspectable var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
            // Optional: You can also add the following line to keep the corners smooth
            layer.masksToBounds = newValue > 0
        }
    }

    // Initializers
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }

    // Setup the view appearance
    private func setupView() {

    }
}
