//
//  AMCVVCProgressBar.swift
//  AMCVVisualComponents
//
//  Created by csharpkid on 04/01/24.
//

import Foundation
import UIKit

open class AMCVVCProgressBar : UIView {
    
    var contentView: UIView!
    
    @IBOutlet weak var slider: UISlider!

    
    @IBInspectable @objc dynamic public var progress: Float {
        set {
            self.slider.setValue(Float(newValue), animated: false)
        }
        get {
            self.slider.value
        }
    }

    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    
    private func commonInit() {
        // Load the XIB
        let bundle = Bundle(for: AMCVVCProgressBar.self)
        let nib = UINib(nibName: "AMCVVCProgressBar", bundle: bundle)
        contentView = nib.instantiate(withOwner: self, options: nil).first as? UIView
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        self.contentView.backgroundColor = AMCVCColors.backgroundBaseColor
        let thumbImage = UIImage(vcName: "AMCVVCFlightThumbnail")
        let lineImage = UIImage(vcName: "AMCVVCFlightThumbnailLine")
        let lineFillImage = UIImage(vcName: "AMCVVCFlightThumbnailFillLine")
        slider.setThumbImage(thumbImage, for: .normal)
        slider.setMinimumTrackImage(lineFillImage, for: .normal)
        slider.setMaximumTrackImage(lineImage, for: .normal)
    }
    
}
