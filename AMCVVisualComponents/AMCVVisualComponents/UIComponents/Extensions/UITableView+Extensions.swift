//
//  UITableView+Extensions.swift
//  AMCVVisualComponents
//
//  Created by csharpkid on 04/01/24.
//

import Foundation
import UIKit

extension UITableView {
    
    // Register a UITableViewCell subclass
    public func register<T: UITableViewCell>(cellType: T.Type,bundle:Bundle) {
         let className = String(describing: cellType)
        let nib = UINib(nibName: className, bundle: bundle)
         self.register(nib, forCellReuseIdentifier: className)
     }

    // Dequeue a reusable cell
    public func dequeueReusableCell<T: UITableViewCell>(for indexPath: IndexPath) -> T? {
        let className = String(describing: T.self)
        guard let cell = dequeueReusableCell(withIdentifier: className, for: indexPath) as? T else {
            return nil
        }
        return cell
    }
}
