//
//  UIFont+Extensions.swift
//  AMCVVisualComponents
//
//  Created by csharpkid on 03/01/24.
//

import Foundation
import UIKit

extension UIFont {

    enum AMCVVCFontStyle: Int {
        case regular = 0
        case semiBold = 1
        case bold = 2

        var fontName: String {
            switch self {
            case .regular:
                return "GarnettRegular-Regular"
            case .semiBold:
                return "GarnettSemibold-Regular"
            case .bold:
                return "Garnett-Bold"
            }
        }
    }

    static func amcvvcFont(style: AMCVVCFontStyle, size: CGFloat) -> UIFont? {
        // Attempt to register the font if it hasn't been loaded yet.
        registerAMCVVCFont(fontName: style.fontName)

        // Return the UIFont object.
        return UIFont(name: style.fontName, size: size)
    }

    private static func registerAMCVVCFont(fontName: String) {
        guard let fontURL = Bundle.init(for: AMCVVCLabel.self).url(forResource: fontName, withExtension: "ttf"),
              let fontDataProvider = CGDataProvider(url: fontURL as CFURL),
              let font = CGFont(fontDataProvider) else {
            print("Could not load font URL for: \(fontName)")
            return
        }

        var error: Unmanaged<CFError>?
        if !CTFontManagerRegisterGraphicsFont(font, &error), let error = error?.takeRetainedValue() {
            if CFErrorGetCode(error) != CTFontManagerError.alreadyRegistered.rawValue {
                return
            }
           
        }
    }
}
