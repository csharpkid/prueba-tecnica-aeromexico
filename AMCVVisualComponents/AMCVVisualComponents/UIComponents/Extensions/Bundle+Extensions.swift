//
//  Bundle+Extensions.swift
//  AMCVVisualComponents
//
//  Created by Dev Boz Mac on 08/01/24.
//

import Foundation


extension Bundle {
    
    static public func getMainBundle()->Bundle {
        return Bundle(for: AMCVVCLabel.self
        )
    }
}
