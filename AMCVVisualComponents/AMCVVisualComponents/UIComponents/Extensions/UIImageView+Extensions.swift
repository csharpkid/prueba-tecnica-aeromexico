//
//  UIImageView+Extensions.swift
//  AMCVVisualComponents
//
//  Created by csharpkid on 04/01/24.
//

import Foundation
import UIKit

extension UIImage {
    
    /// Initializes a UIImage with the specified name and bundle.
    /// - Parameters:
    ///   - named: The name of the image.
    ///   - bundle: The bundle containing the image file or asset catalog. Default is main bundle.
    public convenience init?(vcName: String) {
        self.init(named: vcName, in: Bundle(for: AMCVVCLabel.self), compatibleWith: nil)
    }
}
