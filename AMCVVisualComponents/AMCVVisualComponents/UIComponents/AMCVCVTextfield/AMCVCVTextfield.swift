//
//  AMCVCVTextfield.swift
//  AMCVVisualComponents
//
//  Created by csharpkid on 03/01/24.
//

import Foundation
import UIKit

public protocol AMCVCVTextfieldDelegate: AnyObject {
    func numberDidChange(_ number: String)
    func dateDidSelect(_ date: Date)
}

open class AMCVCVTextfield: UIView, UITextFieldDelegate,UIPickerViewDataSource, UIPickerViewDelegate  {
    
    // MARK: - Properties
    var pickerConstraints:[NSLayoutConstraint] = []
    let placeholderLabel = AMCVVCLabel()
    let prefixLabel = AMCVVCLabel()
    private var onPickerViewSelect: ((Int) -> Void)?

    private var pickerViewDataSource: [String] = []
    private let pickerView = UIPickerView()
    
    let inputTextField = UITextField()
    let calendarIconImageView = UIImageView()
    public weak var delegate: AMCVCVTextfieldDelegate?
    
    @IBInspectable var placeholder: String? {
        didSet {
            placeholderLabel.text = placeholder
        }
    }
    
    
    @IBInspectable var prefix: String? {
        didSet {
            prefixLabel.text = prefix
        }
    }
    
    private func configureToolbar() {
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(dismissKeyboard))
        toolbar.setItems([flexSpace, doneButton], animated: false)
        inputTextField.inputAccessoryView = toolbar
    }
    
    
    @objc private func dismissKeyboard() {
        inputTextField.resignFirstResponder()
    }
    
    @IBInspectable var keyboardType: Int = 0 {
        didSet {
            switch keyboardType {
            case 0:
                inputTextField.inputView = nil
                inputTextField.keyboardType = .numberPad
                calendarIconImageView.isHidden = true
                prefixLabel.isHidden = false
            case 1:
                let datePicker = UIDatePicker()
                datePicker.datePickerMode = .date
                if #available(iOS 13.4, *) {
                    datePicker.preferredDatePickerStyle = .wheels
                } else {
                    // Fallback on earlier versions
                }
                datePicker.addTarget(self, action: #selector(dateChanged(_:)), for: .valueChanged)
                inputTextField.inputView = datePicker
                setDateToToday()
                calendarIconImageView.isHidden = false
                prefixLabel.isHidden = true
                
            case 2:
                prefixLabel.textAlignment = .left
                configurePickerView()
                inputTextField.inputView = pickerView
                calendarIconImageView.isHidden = true
                prefixLabel.isHidden = false
                // Configure any additional settings for the picker view mode
                break
            default:
                break
            }
            setupInitialConstraints()
        }
    }
    
    public func configurePickerViewDataSource(with items: [String],firstConfiguration:Bool? = true, selectionHandler: @escaping (Int) -> Void) {
        pickerViewDataSource = items
        pickerView.reloadAllComponents()
        onPickerViewSelect = selectionHandler
        if firstConfiguration ?? true {
            if !pickerViewDataSource.isEmpty {
                inputTextField.text = items[0]
                pickerView.selectRow(0, inComponent: 0, animated: false)
                onPickerViewSelect?(0) // Call the selection handler with the first item
            }
        }
        
   
    }

    
    private func configurePickerView() {
        pickerView.dataSource = self
        pickerView.delegate = self
    }
    
    
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if keyboardType == 0 { // Numeric input
            let currentText = textField.text ?? ""
            let prospectiveText = (currentText as NSString).replacingCharacters(in: range, with: string)
            if prospectiveText.count <= 3 {
                delegate?.numberDidChange(prospectiveText)
                return true
            } else {
                return false
            }
        }
        return true
    }
    
    private func setDateToToday() {
        if let datePicker = inputTextField.inputView as? UIDatePicker {
            datePicker.date = Date()
            dateChanged(datePicker)
        }
    }
    @objc private func dateChanged(_ sender: UIDatePicker) {
        let formatter = DateFormatter()
        formatter.dateFormat = "EEEE, MMM d"
        inputTextField.text = formatter.string(from: sender.date)
        delegate?.dateDidSelect(sender.date)
        
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    required public init?(coder: NSCoder) {
        super.init(coder: coder)
        setupViews()
    }
    
    public func changePrefix(to newPrefix: String) {
          prefixLabel.text = newPrefix
            
      }
    
    public func configurePickerViewDataSource(with items: [String]) {
        pickerViewDataSource = items
        pickerView.reloadAllComponents()
    }

    
    // MARK: - Setup Views
    private func setupViews() {
        placeholderLabel.text = "Placeholder..."
        placeholderLabel.customFontSize = 10
        placeholderLabel.fontStyle = 0
        placeholderLabel.translatesAutoresizingMaskIntoConstraints = false
        prefixLabel.text = "MX"
        prefixLabel.customFontSize = 16
        prefixLabel.fontStyle = 1
        prefixLabel.textColor =  AMCVCColors.disabledTextColor
        prefixLabel.translatesAutoresizingMaskIntoConstraints = false
        addSubview(prefixLabel)
        addSubview(placeholderLabel)
        inputTextField.text = "000"
        inputTextField.font = UIFont.amcvvcFont(style: .semiBold, size: 16)
        inputTextField.keyboardType = .numberPad
        inputTextField.tintColor = UIColor.clear
        inputTextField.borderStyle = .none
        self.layer.borderColor = AMCVCColors.tintColor.cgColor
        self.layer.borderWidth = 2
        self.layer.cornerRadius = 12
        inputTextField.translatesAutoresizingMaskIntoConstraints = false
        addSubview(inputTextField)
        configureCalendarIconImageView()
        configureToolbar()
        inputTextField.delegate = self
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(onTapped))
        self.addGestureRecognizer(tapGesture)
    }
    
    @objc func onTapped(){
        if keyboardType == 0 {
            self.inputTextField.selectAll(nil)
        }
        self.inputTextField.becomeFirstResponder()
        
    }
    
    private func setupInitialConstraints() {
        switch keyboardType {
        case 0:
            NSLayoutConstraint.activate([
                placeholderLabel.topAnchor.constraint(equalTo: self.topAnchor,constant: 15),
                placeholderLabel.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 15),
                placeholderLabel.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -15),
                prefixLabel.topAnchor.constraint(equalTo: self.placeholderLabel.bottomAnchor,constant: 0),
                prefixLabel.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 15),
                
            ])
            NSLayoutConstraint.activate([
                inputTextField.topAnchor.constraint(equalTo: placeholderLabel.bottomAnchor, constant: -13),
                inputTextField.leadingAnchor.constraint(equalTo: self.prefixLabel.trailingAnchor, constant: 2),
                inputTextField.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -30),
                inputTextField.bottomAnchor.constraint(equalTo: self.bottomAnchor)
            ])
            break
        case 1:
            NSLayoutConstraint.activate([
                placeholderLabel.topAnchor.constraint(equalTo: self.topAnchor,constant: 15),
                placeholderLabel.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 15),
                placeholderLabel.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: 15),
                placeholderLabel.bottomAnchor.constraint(equalTo: self.inputTextField.topAnchor,constant: 0),
                calendarIconImageView.widthAnchor.constraint(equalToConstant: 20),
                calendarIconImageView.heightAnchor.constraint(equalToConstant: 20),
                calendarIconImageView.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -15),
                calendarIconImageView.centerYAnchor.constraint(equalTo: self.centerYAnchor),
                inputTextField.leadingAnchor.constraint(equalTo: self.placeholderLabel.leadingAnchor, constant: 0),
            ])
            break
            
        case 2:
             pickerConstraints =  [
                placeholderLabel.topAnchor.constraint(equalTo: self.topAnchor, constant: 15),
                placeholderLabel.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 15),
                placeholderLabel.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -15),
                inputTextField.topAnchor.constraint(equalTo: placeholderLabel.bottomAnchor, constant: 0),
                inputTextField.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 15),
                inputTextField.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -13), 
                prefixLabel.leadingAnchor.constraint(equalTo: inputTextField.trailingAnchor, constant: 2),
                prefixLabel.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -15),
                prefixLabel.centerYAnchor.constraint(equalTo: inputTextField.centerYAnchor)
            ]
            NSLayoutConstraint.activate(pickerConstraints)
            break
        default:
            break
        }
    }
    
    private func configureCalendarIconImageView() {
        calendarIconImageView.image = UIImage(named: "AMCVVCCalendar", in: Bundle.init(for: type(of: self)), with: nil)
        calendarIconImageView.contentMode = .scaleAspectFit
        calendarIconImageView.isHidden = true
        calendarIconImageView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(calendarIconImageView)
    }
    

    public func numberOfComponents(in pickerView: UIPickerView) -> Int {
         return 1
     }

    public func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
         return pickerViewDataSource.count
     }

    public func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
         return pickerViewDataSource[row]
     }
    public func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if let selectedText = pickerViewDataSource[safe: row] {
            inputTextField.text = selectedText
            onPickerViewSelect?(row)
            NSLayoutConstraint.deactivate(self.pickerConstraints)
            self.setupInitialConstraints()
            self.layoutIfNeeded()
        }
    }
}

fileprivate extension Collection {

    subscript(safe index: Index) -> Element? {
        return indices.contains(index) ? self[index] : nil
    }
    
}
