//
//  AMCVCVButton.swift
//  AMCVVisualComponents
//
//  Created by csharpkid on 03/01/24.
//

import Foundation
import UIKit

class AMCVCVButton: UIButton {
    
    // Observable and inspectable corner radius property
    @IBInspectable @objc dynamic var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
            layer.masksToBounds = newValue > 0
        }
    }

    // Additional initialization can go here
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    private func commonInit() {
        // Any additional setup can be done here
        // For example, setting default styles for the button
        //self.titleLabel?.font = UIFont.amcvvcFont(style: .semiBold, size: 16)
        self.tintColor = AMCVCColors.backgroundBaseColor
        self.backgroundColor = AMCVCColors.tintColor
    }
}
