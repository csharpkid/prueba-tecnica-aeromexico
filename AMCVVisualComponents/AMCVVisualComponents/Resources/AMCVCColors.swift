//
//  AMCVCColors.swift
//  AMCVVisualComponents
//
//  Created by csharpkid on 02/01/24.
//

import Foundation
import UIKit

public class AMCVCColors {
    public static let tintColor:UIColor = UIColor(red: 0.00, green: 0.00, blue: 0.00, alpha: 1.00)
    
    public static let backgroundBaseColor:UIColor = UIColor(red: 1.00, green: 1.00, blue: 1.00, alpha: 1.00)
    
    public static let backgroundSecundaryColor:UIColor = UIColor(red: 0.969, green: 0.969, blue: 0.969, alpha: 1)
    
    public static let borderColor:UIColor = UIColor(red: 0.839, green: 0.839, blue: 0.839, alpha: 1)
    public static let disabledTextColor:UIColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.3)
    public static let semiDisabledTextColor:UIColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.4)

    
    
}
