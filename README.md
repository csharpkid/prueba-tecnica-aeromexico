# Proyecto iOS para Aeroméxico

Este proyecto está adaptado para dispositivos iOS que ejecuten la versión 14.0 o superior de iOS.

## Colaboradores
- **Cristian Villegas**

## Desafío

## P: ¿Cuál fue el mayor reto en esta prueba?

R: El mayor desafío consistió en lograr un equilibrio donde la solución se comparara con el ensamble de piezas de Lego, pero a nivel de Frameworks y/o ideas de negocio. El foco no solo estuvo en resolver un problema, sino en construir una base que promoviera la escalabilidad. Con un marco de trabajo que incorpora una arquitectura ViewEntity->VIPER/MVVM, se buscó un desarrollo ágil e independiente de servicios. Este enfoque se diseñó pensando en la expansión a gran escala, facilitando la colaboración de un equipo extenso, y con la flexibilidad necesaria para adaptarse a tecnologías emergentes como Swift Package Manager o estructuras Multi-Repo basadas en xcframeworks y frameworks abiertos. Todo esto apoyado en un core que se puede escalar de manera eficiente.

Además, esta estructura permite implementar procesos de Integración y Despliegue Continuos (CI/CD) de manera independiente para los frameworks, los equipos y el core del proyecto. Esto no solo optimiza el flujo de trabajo, sino que también habilita un versionamiento autónomo por frameworks o a través del Swift Package Manager, lo que refuerza la modularidad y la agilidad en el desarrollo y lanzamiento de nuevas características.

## Descripción del Proyecto

Este repositorio contiene el código fuente para una prueba técnica llevada a cabo para el puesto de Líder Técnico iOS en Aeroméxico.

El código demuestra competencia en prácticas modernas de arquitectura iOS.

## Arquitecturas utilizadas
- **MVVM**
- **ViewEntity + VIPER**

## Requisitos

- iOS 14.0+
- Xcode 12.0+
- Swift 5.0+

## Configuración Sin Widgets
- **El scheme principal a correr es AMCVFront**

Para ejecutar este proyecto, clona el repositorio y abre el espacio de trabajo en Xcode.

```bash
git clone https://github.com/tu-url-del-repositorio.git
cd tu-directorio-del-proyecto
open AMCVCoreFrameworks.xcworkspace
```
## Configuracion Con Widgets

<img src="RDAssets/wg-01.png" alt="Image" height="500"  style="width: 100%; height: auto;"/>
<img src="RDAssets/wg-01fav.png" alt="Image" height="500"  style="width: 100%; height: auto;"/>


- Paso 1: Cambiarse al esquema AMCVFront w/ Widgets.

**Esto  correra la App y automaticamente te añadira el widget de la aplicacion**

<img src="RDAssets/wg-scheme.png" alt="Image" width="600" />


- Paso 2: Activar App Groups en WidgetExtension en **AMCVFront** y **AMCVWidgetsExtension.**
- También es necesario seleccionar un equipo de desarrollo que cuente con capacidad para App Groups, debido a las restricciones impuestas por Apple.

<img src="RDAssets/wg-02-def.png" alt="Image" width="600" />
<img src="RDAssets/wg-03-def.png" alt="Image" width="600" />


- Es importante Activar el siguiente App Group Antes de correr el widget en **AMCVFront** y **AMCVWidgetsExtension.**

```bash
group.cristianvillegas.com
```

**Este paso es esencial para el correcto funcionamiento de la persistencia en los widgets y la app.** 

## Agradecimientos

**Quiero expresar mi más sincero agradecimiento por la atención prestada y el tiempo dedicado a evaluar este Proyecto.**


**Doc Created by: Cristian Eduardo Villegas Alvarez**
