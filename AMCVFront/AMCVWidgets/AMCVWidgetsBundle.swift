//
//  AMCVWidgetsBundle.swift
//  AMCVWidgets
//
//  Created by csharpkid on 03/01/24.
//
import WidgetKit
import SwiftUI
import AMCVFlights
import AMCVVisualComponents

@main
struct AMCVWidgetsBundle: WidgetBundle {
    @WidgetBundleBuilder
    var body: some Widget {
        SimpleWidget()// This will make the widget available only in the small size

    }
}

struct SimpleWidget: Widget {
    let kind: String = "simple_widget"

    var body: some WidgetConfiguration {
        StaticConfiguration(kind: kind, provider: SimpleProvider()) { entry in
         
            SimpleWidgetEntryView(entry: entry).padding(EdgeInsets(top: 0, leading: 0, bottom: 0, trailing: 0))
        }
        .configurationDisplayName("Favorite Flights").supportedFamilies([.systemSmall])
    }
}


struct SimpleProvider: TimelineProvider {
    func placeholder(in context: Context) -> SimpleEntry {
        SimpleEntry(date: Date(), flights: [])
    }

    func getSnapshot(in context: Context, completion: @escaping (SimpleEntry) -> ()) {
        let flights = AMCVFavoritesManager.shared.getFlightInfo()
        let entry = SimpleEntry(date: Date(), flights: flights)
        completion(entry)
    }

    func getTimeline(in context: Context, completion: @escaping (Timeline<Entry>) -> ()) {
        var entries: [SimpleEntry] = []
        let currentDate = Date()
        let flights = AMCVFavoritesManager.shared.getFlightInfo()
        let entryDate = Calendar.current.date(byAdding: .hour, value: 1, to: currentDate)!
        let entry = SimpleEntry(date: entryDate, flights: flights)
        entries.append(entry)
        let timeline = Timeline(entries: entries, policy: .after(entryDate))
        completion(timeline)
    }
}

struct SimpleEntry: TimelineEntry {
    let date: Date
    let flights: [FlightInfo]
}

struct SimpleWidgetEntryView: View {
    var entry: SimpleProvider.Entry

    var body: some View {
  
        ZStack {
            if let firstFlight = entry.flights.first {
                ZStack(alignment: .topTrailing) {
                    let status = AMCVFlightStatusType(rawValue: firstFlight.flightStatus) ?? .arrived
                    switch status {
                    case .arrived:
                        Image("AMCVVCArrivedPill",bundle: Bundle.getMainBundle()).resizable()
                            .scaledToFit()
                            .frame(width: 100, height: 40)
                            .clipped()
                            .offset(x: -38, y: -70)
                            .zIndex(1)
                    case .onTime:
                        Image("AMCVVCOnTimePill",bundle: Bundle.getMainBundle()).resizable()
                            .scaledToFit()
                            .frame(width: 100, height: 40)
                            .clipped()
                            .offset(x: -38, y: -70)
                            .zIndex(1)
                    case .delayed:
                        Image("AMCVVCDelayedPill",bundle: Bundle.getMainBundle()).resizable()
                            .scaledToFit()
                            .frame(width: 100, height: 40)
                            .clipped()
                            .offset(x: -38, y: -70)
                            .zIndex(1)
                    }
                }
                VStack(spacing: 10) {
                    Text("AM")
                        .bold() // Make "AM" bold
                        .foregroundColor(.black).font(.system(size: 18)) +
                    Text(firstFlight.flightNumber)
                        .foregroundColor(.black).font(.system(size: 18))
                    Text("Boarding:")
                        .bold() // Make "AM" bold
                        .foregroundColor(.black).font(.system(size: 16))
                    Text(firstFlight.boardingTime)
                        .padding(8)
                        .background(Color.black)
                        .cornerRadius(10)
                        .foregroundColor(.white)
                        .font(.system(size: 16))
                }.padding(.top,20)
            } else
            {
                VStack {
                    Text("Add some flights to favorite...")
                        .padding(8)
                        .multilineTextAlignment(.center)
                        .background(Color.black)
                        .cornerRadius(10)
                        .foregroundColor(.white)
                        .font(.system(size: 14))
                }
            }

        }
    }
}


