//
//  AMCVNotificationsManager.swift
//  AMCVFront
//
//  Created by csharpkid on 04/01/24.
//

import Foundation
import UserNotifications
struct NotificationUserInfoKey {
    static let title = "title"
    static let body = "body"
}
class AMCVNotificationsManager {

    // Shared instance for singleton usage (optional)
    static let shared = AMCVNotificationsManager()

    // Method to request notification permission
    func requestNotificationAuthorization(delegate: UNUserNotificationCenterDelegate) {
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) { granted, error in
            if granted {
                UNUserNotificationCenter.current().delegate = delegate
                NotificationCenter.default.addObserver(self, selector: #selector(self.handleCustomNotification(_:)), name: .customNotification, object: nil)
            } else if let error = error {
                print("Notification permission denied due to: \(error.localizedDescription).")
            }
        }
    }

    // Method to schedule a notification

    @objc private func handleCustomNotification(_ notification: Notification) {
         if let userInfo = notification.userInfo as? [String: Any],
            let title = userInfo[NotificationUserInfoKey.title] as? String,
            let body = userInfo[NotificationUserInfoKey.body] as? String {
             launchNotification(withTitle: title, body: body)
         }
     }

    
    func launchNotification(withTitle title: String, body: String) {
        let center = UNUserNotificationCenter.current()
        center.getNotificationSettings { settings in
            guard settings.authorizationStatus == .authorized else {
                print("Notifications are not allowed.")
                return
            }

            let content = UNMutableNotificationContent()
            content.title = title
            content.body = body
            content.sound = UNNotificationSound.default

            // No trigger is needed for immediate delivery
            let request = UNNotificationRequest(identifier: UUID().uuidString, content: content, trigger: nil)

            center.add(request) { error in
                if let error = error {
                    print("Error scheduling immediate notification: \(error.localizedDescription)")
                } else {
                    print("Immediate notification scheduled successfully.")
                }
            }
        }
    }

    
}

extension Notification.Name {
    static let customNotification = Notification.Name("customNotification")
}
