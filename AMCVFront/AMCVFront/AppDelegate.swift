import UIKit
import AMCVNavigator
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,UNUserNotificationCenterDelegate {
    
    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Initialize your window
        window = UIWindow(frame: UIScreen.main.bounds)
        // Create an instance of your desired view controller
        let initialViewController = UINavigationController(rootViewController: AMCVFLauncherRouter.createModule())
        window?.rootViewController = initialViewController
        // Make the window visible
        window?.makeKeyAndVisible()
        AMCVNotificationsManager.shared.requestNotificationAuthorization(delegate: self)
        return true
    }
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        // Show the notification while the app is in the foreground
        completionHandler([.banner, .sound])
    }
}
